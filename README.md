<h1 align="center">CODE REACT Pre Module Layout</h1>
<h3 align="center">My React template for use in codecorp.com website</h3>

## Description

`create-react-app` is just a little too large to show the basic layout for structuring your Html parts for website integration.

## Folder Structure

```
basic-react/
  📦public
    ┗ 📜index.html
  📦src
    ┣ 📜App.js
    ┗ 📜index.js
```

## Getting Started

This layout is only to show you how to structure your Html view so it can be pulled from the repo and integrated into a laravel module. 

The public/index.html has our base theme style and assets for you to use in your design. 

Do not hardcode these assets into your design; simply add them to temp files that can be removed or commented out when integrated into the website. 

When integrated into the module, the assets, images, header, and footer will come from the main site.

The main section of your app needs to be placed inside this div. 

```
<div class="container-fluid">

</div>
```
Here are the assets to use if you want to create your own just remember once you want to have it used all we need is your app wrapped in the div above but designed with the below assets. Remember do not include them in a hardcoded way as they will be removed on module integration.

> < head >  Head Section Code


``` 

<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light" rel="stylesheet" type="text/css">
<link href="https://fonts.googleapis.com/css2?family=Archivo+Black&family=Permanent+Marker&family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css2?family=Inconsolata:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet">
<link rel="stylesheet" href="https://codecorp.com/site/vendor/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="https://codecorp.com/site/vendor/fontawesome-free/css/all.min.css">
<link rel="stylesheet" href="https://codecorp.com/site/vendor/animate/animate.min.css">
<link rel="stylesheet" href="https://codecorp.com/site/vendor/simple-line-icons/css/simple-line-icons.min.css">
<link rel="stylesheet" href="https://codecorp.com/site/vendor/owl.carousel/assets/owl.carousel.min.css">
<link rel="stylesheet" href="https://codecorp.com/site/vendor/owl.carousel/assets/owl.theme.default.min.css">
<link rel="stylesheet" href="https://codecorp.com/site/vendor/magnific-popup/magnific-popup.min.css">
<link rel="stylesheet" href="https://codecorp.com/site/vendor/bootstrap-star-rating/css/star-rating.min.css">
<link rel="stylesheet" href="https://codecorp.com/site/vendor/bootstrap-star-rating/themes/krajee-fas/theme.min.css">
<link rel="stylesheet" href="https://codecorp.com/site/css/bootstrap4-toggle.min.css">
<link rel="stylesheet" href="https://codecorp.com/site/css/modal-video.min.css">
<link rel="stylesheet" href="https://codecorp.com/site/css/theme.css">
<link rel="stylesheet" href="https://codecorp.com/site/css/theme-elements.css">
<link rel="stylesheet" href="https://codecorp.com/site/css/theme-blog.css">
<link rel="stylesheet" href="https://codecorp.com/site/css/theme-shop.css">
<link rel="stylesheet" href="https://codecorp.com/site/vendor/circle-flip-slideshow/css/component.css">
<link rel="stylesheet" href="https://codecorp.com/site/css/default.css">
<link rel="stylesheet" href="https://codecorp.com/site/css/responsive.css">

```

> Footer Js Assets

```

<script src="https://codecorp.com/site/vendor/jquery/jquery.min.js"></script>
<script src="https://codecorp.com/site/vendor/jquery.appear/jquery.appear.min.js"></script>
<script src="https://codecorp.com/site/vendor/jquery.easing/jquery.easing.min.js"></script>
<script src="https://codecorp.com/site/vendor/jquery.cookie/jquery.cookie.min.js"></script>
<script src="https://codecorp.com/site/vendor/popper/umd/popper.min.js"></script>
<script src="https://codecorp.com/site/vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="https://codecorp.com/site/vendor/jquery.validation/jquery.validate.min.js"></script>
<script src="https://codecorp.com/site/vendor/jquery.easy-pie-chart/jquery.easypiechart.min.js"></script>
<script src="https://codecorp.com/site/vendor/jquery.gmap/jquery.gmap.min.js"></script>
<script src="https://codecorp.com/site/vendor/lazysizes/lazysizes.min.js"></script>
<script src="https://codecorp.com/site/js/isotope.pkgd.js"></script>
<script src="https://codecorp.com/site/js/packery.pkgd.min.js"></script>
<script src="https://codecorp.com/site/js/infinite-scroll.pkgd.min.js"></script>
<script src="https://codecorp.com/site/vendor/owl.carousel/owl.carousel.min.js"></script>
<script src="https://codecorp.com/site/vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
<script src="https://codecorp.com/site/vendor/vide/jquery.vide.min.js"></script>
<script src="https://codecorp.com/site/vendor/vivus/vivus.min.js"></script>
<script src="https://codecorp.com/site/js/jquery-modal-video.min.js"></script>
<script src="https://codecorp.com/site/vendor/bootstrap-star-rating/js/star-rating.min.js"></script>
<script src="https://codecorp.com/site/vendor/bootstrap-star-rating/themes/krajee-fas/theme.min.js"></script>
<script src="https://codecorp.com/site/vendor/jquery.countdown/jquery.countdown.min.js"></script>
<script src="https://codecorp.com/site/vendor/elevatezoom/jquery.elevatezoom.min.js"></script>
<script src="https://codecorp.com/site/js/bootstrap4-toggle.min.js"></script>
<script src="https://codecorp.com/site/js/jquery.equalheights.js"></script>
<script src="https://codecorp.com/site/js/theme.js"></script>
<script src="https://codecorp.com/site/vendor/circle-flip-slideshow/js/jquery.flipshow.min.js"></script>
<script src="https://codecorp.com/site/js/views/view.home.js"></script>
<script src="https://codecorp.com/site/js/theme.init.js"></script>
<script src="https://codecorp.com/site/js/examples/examples.gallery.js"></script>     
<script src="https://codecorp.com/site/js/custom.js"></script>

```



## Technologies

-   [React](https://reactjs.org/)

## Plans

-   [ ] Add a basic laravel module for you to see what this will integrate into. 

## Support

If you need more help or have questions contact me at phillip.madsen@codecorp.com.
