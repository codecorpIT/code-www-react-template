# WEBSITE INTEGRATION DOC!
This document is for requesting the integration of your app into the codecorp.com website.  The more detail you provide, the faster it can be approved and integrated.  This document allows other teams involved to get what they need figured out or done to make your request happen.

## APP TITLE: {add your title here}

 [YOUR REPO NAME HER](https://bitbucket.org/codecorpIT/code-www-react-template/src/develop/)


### App Description:
{add your description  here}


### App Integration Reason:
Example:
This will allow our visitors / clients to generate the qr codes needed for programming their CR2700. 

### App Permissions:
Example: 
This app will engage with logged in users. 
They need to access this with mycode user permissions
 
### Link Details:
**Please tell us how you see this app being reached after integration.**
Example:
We would like to put this in the support section of the site. 

#### Preferred URL: https://codecorp.com/support/{your-app}
#### Navigation Menu Request:
Example:
Add the link in support under patent marketing

## Technologies

-   [React](https://reactjs.org/)

## Plans

-   [ ] add steps for upcoming work we an prepare for.

## CICD 
Do you need this to be moderated with other people outside of the websites CICD pipleline? If you do please list the additional steps below. 

## Support
**If you need more help or have questions contact me at phillip.madsen@codecorp.com.**